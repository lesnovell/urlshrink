
<h1>About UrlShrink</h1>
UrlShrink is a sample app that allows encoding of long URLs into shorter ones.
<p/>

<h2>Running Application</h2>
<ul>
    <li><a href="http://urlshrink.novell.tech">Open URLShrink Demo at http://urlshrink.novell.tech</a></li>
</ul>

<h2>Source Repository</h2>
<ul>
    <li><a href="https://gitlab.com/lesnovell/urlshrink">Gitlab https://gitlab.com/lesnovell/urlshrink</a></li>
</ul>

<h2>Backend Details</h2>
<ul>
    <li>Language: Java</li>
    <li>Tools: Spring</li>
    <li>Database: Redis</li>
    <li>Web Service: GraphQL and REST available. GraphQL documentation is accessible from the <b>GraphiQL</b> tab on the left.</li>
</ul>

<h2>Frontend Details</h2>
<ul>
    <li>Language: Typescript</li>
    <li>Tools: NodeJS, React, NextJS</li>
    <li>GraphQL: Uses Apollo GraphQL to connect to the backend</li>
</ul>

<h2>DevOps Details</h2>
<ul>
    <li>Gitlab automated CI builds, dockerizes, and publishes releases</li>
    <li>Terraform config to create VPC, Redis Database, EC2 for front-end, and EC2 for back-end</li>
</ul>
