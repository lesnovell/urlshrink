
resource "aws_elb" "frontend_elb" {
  security_groups = ["${aws_security_group.prod-security-group.id}"]
  subnets = ["${aws_subnet.prod-subnet-public-1.id}"]

  "listener" {
    instance_port = 8080
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Application   = "UrlShrink"
  }
}

resource "aws_launch_configuration" "frontend_launch_configuration" {
  image_id = "ami-0a887e401f7654935"
  instance_type = "t3.medium"
  enable_monitoring = "true"
  security_groups = ["${aws_security_group.prod-security-group.id}"]
  key_name = "personalkeypair"
  user_data = <<EOF
#!/bin/bash
sudo yum update -y
sudo yum install docker -y
sudo dockerd &
sudo docker run -p8080:3000 -e "NODE_ENV=production" lesnovell/urlshrinkfrontend:latest
echo done
EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "frontend_autoscaling_group" {
  name = "frontend_autoscaling_group-${aws_launch_configuration.frontend_launch_configuration.name}"
  depends_on = ["aws_launch_configuration.frontend_launch_configuration"]
  launch_configuration = "${aws_launch_configuration.frontend_launch_configuration.id}"
  max_size = 1
  min_size = 1
  desired_capacity = 1
  load_balancers = ["${aws_elb.frontend_elb.id}"]
  force_delete = true

  lifecycle {
    create_before_destroy = true
  }

  vpc_zone_identifier = ["${aws_subnet.prod-subnet-public-1.id}"]
}
