output frontend_elb_dns_name {
  value = "${aws_elb.frontend_elb.dns_name}"
}
output backend_elb_dns_name {
  value = "${aws_elb.backend_elb.dns_name}"
}
output "redis_dns_name" {
  value = "${aws_elasticache_replication_group.urlshrink_redis.primary_endpoint_address}"
}