resource "aws_elasticache_replication_group" "urlshrink_redis" {
  replication_group_id          = "urlshrink"
  replication_group_description = "UrlShrink Database"
  engine_version                = "3.2.4"
  node_type                     = "cache.t2.medium"
  number_cache_clusters         = "1"
  port                          = "6379"
  parameter_group_name          = "default.redis3.2"
  apply_immediately             = "true"
  security_group_ids = ["${aws_security_group.redis-security-group.id}"]
  subnet_group_name = "${aws_elasticache_subnet_group.redis-subnet.name}"

  tags {
    Application   = "UrlShrink"
  }
}
