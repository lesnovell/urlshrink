package com.novell.urlshrink.service;

import com.novell.urlshrink.data.UrlShrinkDatabase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ShortPathGeneratorByCounterTest {
    @Mock
    public UrlShrinkDatabase urlShrinkDatabase;

    @InjectMocks
    public ShortPathGenerator in = new ShortPathGeneratorByCounter();

    @Test
    public void generateShortPath() {
        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(0));
        assertEquals("000", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(1));
        assertEquals("001", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(10));
        assertEquals("00a", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(64));
        assertEquals("00_", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(65));
        assertEquals("010", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(130));
        assertEquals("020", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(9873));
        assertEquals("2lW", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(30000));
        assertEquals("76z", in.generateShortPath());

        Mockito.when(urlShrinkDatabase.nextShortUrlIndex()).thenReturn(new Long(274625));
        assertEquals("1000", in.generateShortPath());
    }
}