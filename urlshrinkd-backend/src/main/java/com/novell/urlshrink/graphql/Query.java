package com.novell.urlshrink.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.novell.urlshrink.entity.ShortUrlMapping;
import com.novell.urlshrink.error.NotFoundException;
import com.novell.urlshrink.service.ShortUrlService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Optional;

@Component
public class Query implements GraphQLQueryResolver {
    @Resource
    private ShortUrlService shortUrlService;

    public ShortUrlMapping getMapping(String shortPath) {
        Optional<ShortUrlMapping> shortUrlMapping = shortUrlService.getMapping(shortPath);
        if (!shortUrlMapping.isPresent()) {
            throw new NotFoundException(shortPath + " could not be found");
        }

        return shortUrlMapping.get();
    }
}