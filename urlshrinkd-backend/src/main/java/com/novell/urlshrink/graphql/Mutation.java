package com.novell.urlshrink.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.novell.urlshrink.entity.ShortUrlMapping;
import com.novell.urlshrink.error.NotFoundException;
import com.novell.urlshrink.service.ShortUrlService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class Mutation implements GraphQLMutationResolver {
    @Resource
    private ShortUrlService shortUrlService;

    public ShortUrlMapping createMapping(String longUrl) {
        return shortUrlService.createMapping(longUrl);
    }

    public Boolean deleteMapping(String shortPath) {
        if (!shortUrlService.deleteMapping(shortPath)) {
            throw new NotFoundException(shortPath + " cannot be found");
        }
        return Boolean.TRUE;
    }
}