package com.novell.urlshrink.service;

public interface ShortPathGenerator {
    String generateShortPath();
}
