package com.novell.urlshrink.service;

import com.novell.urlshrink.entity.ShortUrlMapping;

import java.util.Optional;

public interface ShortUrlService {
    ShortUrlMapping createMapping(String longUrl);
    Optional<ShortUrlMapping> getMapping(String shortPath);
    boolean deleteMapping(String shortPath);
}
