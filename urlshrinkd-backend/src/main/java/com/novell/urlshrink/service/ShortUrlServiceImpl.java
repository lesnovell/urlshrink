package com.novell.urlshrink.service;

import com.novell.urlshrink.UrlShrinkConfiguration;
import com.novell.urlshrink.data.UrlShrinkDatabase;
import com.novell.urlshrink.entity.ShortUrlMapping;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class ShortUrlServiceImpl implements ShortUrlService {
    @Resource
    private UrlShrinkConfiguration urlShrinkConfiguration;

    @Resource
    private UrlShrinkDatabase urlShrinkDatabase;

    @Resource
    private ShortPathGenerator shortPathGenerator;

    @Override
    public ShortUrlMapping createMapping(String longUrl) {
        String shortPath = shortPathGenerator.generateShortPath();
        return urlShrinkDatabase.create(shortPath, longUrl);
    }

    @Override
    public Optional<ShortUrlMapping> getMapping(String shortPath) {
        Optional<ShortUrlMapping> optional = urlShrinkDatabase.find(shortPath);

        if (optional.isPresent()) {
            ShortUrlMapping shortUrlMapping = optional.get();
            if (shortUrlMapping.getUseCount() == urlShrinkConfiguration.getMapping().getMaxUseCount()) {
                urlShrinkDatabase.delete(shortPath);
            }
        }
        return optional;
    }

    @Override
    public boolean deleteMapping(String shortPath) {
        boolean deleted = false;

        if (urlShrinkDatabase.find(shortPath).isPresent()) {
            urlShrinkDatabase.delete(shortPath);
            deleted = true;
        }

        return deleted;
    }
}
