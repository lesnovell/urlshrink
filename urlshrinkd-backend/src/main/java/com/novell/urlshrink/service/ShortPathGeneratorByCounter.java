package com.novell.urlshrink.service;

import com.novell.urlshrink.data.UrlShrinkDatabase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ShortPathGeneratorByCounter implements ShortPathGenerator {
    // 65
    private static Character[] VALID_CHARS =
            {'0','1','2','3','4','5','6','7','8','9',
             'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
             'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
             '-','.','_'};

    @Resource
    private UrlShrinkDatabase urlShrinkDatabase;

    public String generateShortPath() {
        int index = (int) urlShrinkDatabase.nextShortUrlIndex();
        StringBuffer path = new StringBuffer();
        path(path, index);

        return StringUtils.leftPad(path.toString(), 3, '0');
  }

    private void path(StringBuffer path, int index) {
        if (index / 65 > 0) {
            path(path,index / 65);
        }

        path.append(VALID_CHARS[index % 65]);
    }
}
