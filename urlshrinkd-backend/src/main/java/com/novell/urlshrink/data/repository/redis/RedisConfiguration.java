package com.novell.urlshrink.data.repository.redis;

import com.novell.urlshrink.UrlShrinkConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import javax.annotation.Resource;

@EnableRedisRepositories
@Configuration
public class RedisConfiguration {
    @Resource private UrlShrinkConfiguration urlShrinkConfiguration;

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration config =
                new RedisStandaloneConfiguration(urlShrinkConfiguration.getRedis().getHost(),
                        urlShrinkConfiguration.getRedis().getPort());

        return new JedisConnectionFactory(config);
    }
}