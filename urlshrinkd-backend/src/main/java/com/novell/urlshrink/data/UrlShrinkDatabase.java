package com.novell.urlshrink.data;

import com.novell.urlshrink.entity.ShortUrlMapping;

import java.util.Optional;

public interface UrlShrinkDatabase {
    ShortUrlMapping create(String shortPath, String longUrl);
    Optional<ShortUrlMapping> find(String shortPath);
    void delete(String shortPath);
    long nextShortUrlIndex();
}
