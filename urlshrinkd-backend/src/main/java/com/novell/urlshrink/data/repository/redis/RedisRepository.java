package com.novell.urlshrink.data.repository.redis;

import com.novell.urlshrink.entity.ShortUrlMapping;
import org.springframework.data.repository.CrudRepository;

public interface RedisRepository extends CrudRepository<ShortUrlMapping, String> {
}