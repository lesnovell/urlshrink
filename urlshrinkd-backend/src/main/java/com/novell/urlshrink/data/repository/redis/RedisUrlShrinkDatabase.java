package com.novell.urlshrink.data.repository.redis;

import com.novell.urlshrink.data.UrlShrinkDatabase;
import com.novell.urlshrink.entity.ShortUrlMapping;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class RedisUrlShrinkDatabase implements UrlShrinkDatabase {
    @Resource private RedisRepository repository;
    private RedisAtomicLong counter;

    public RedisUrlShrinkDatabase(RedisConnectionFactory connectionFactory) {
        counter = new RedisAtomicLong("UrlShrinkCounter", connectionFactory);
    }

    @Override
    public ShortUrlMapping create(String shortPath, String longUrl) {
        return repository.save(new ShortUrlMapping(shortPath, longUrl, 0));
    }

    @Override
    public Optional<ShortUrlMapping> find(String shortPath) {
        Optional<ShortUrlMapping> optional = repository.findById(shortPath);

        if (optional.isPresent()) {
            ShortUrlMapping shortUrlMapping = optional.get();
            shortUrlMapping.setUseCount(shortUrlMapping.getUseCount()+1);
            repository.save(shortUrlMapping);
        }

        return optional;
    }

    @Override
    public void delete(String shortPath) {
        repository.deleteById(shortPath);
    }

    @Override
    public long nextShortUrlIndex() {
        return counter.getAndIncrement();
    }
}
