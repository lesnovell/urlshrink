package com.novell.urlshrink.controller;

import com.novell.urlshrink.entity.ShortUrlMapping;
import com.novell.urlshrink.error.BadRequestException;
import com.novell.urlshrink.error.NotFoundException;
import com.novell.urlshrink.service.ShortUrlService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

@Controller
@RequestMapping(value = "/v1/api/shorturl", produces = "application/json", consumes = "application/json")
public class ShortUrlController {

    @Resource
    private ShortUrlService shortUrlService;

    @CrossOrigin
    @PostMapping
    public ResponseEntity<ShortUrlMapping> create(@RequestParam("longUrl") String longUrl) {
        if (StringUtils.isEmpty(longUrl)) throw new BadRequestException("longUrl must be not be empty");
        return new ResponseEntity<>(shortUrlService.createMapping(longUrl), HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping("{shortPath}")
    @ResponseBody
    public void delete(@PathVariable("shortPath") String shortPath) {
        if (StringUtils.isEmpty(shortPath)) throw new BadRequestException("shortPath must be not be empty");

        if (!shortUrlService.deleteMapping(shortPath))
            throw new NotFoundException(shortPath + " cannot be found");
    }

    @CrossOrigin
    @GetMapping("{shortPath}")
    @ResponseBody
    public ShortUrlMapping get(@PathVariable("shortPath") String shortPath) {
        Optional<ShortUrlMapping> shortUrlMapping = shortUrlService.getMapping(shortPath);
        if (!shortUrlMapping.isPresent()) {
            throw new NotFoundException("Mapping not found for " + shortPath);
        }

        return shortUrlMapping.get();
    }
}
