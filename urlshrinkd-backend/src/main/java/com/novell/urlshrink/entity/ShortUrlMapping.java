package com.novell.urlshrink.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.stereotype.Indexed;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("ShortUrlMapping")
public class ShortUrlMapping {
    @Id private String shortPath;
    private String longUrl;
    private Integer useCount;
}
