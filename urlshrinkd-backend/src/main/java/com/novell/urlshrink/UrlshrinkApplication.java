package com.novell.urlshrink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlshrinkApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrlshrinkApplication.class, args);
    }

}
