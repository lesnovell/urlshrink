import {createServer, IncomingMessage, ServerResponse} from 'http'
import { parse } from 'url'
import next from 'next'
import {redirectHandler} from "../src/urlshrink/RedirectHandler";

const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const nextJsRequestHandler = app.getRequestHandler();

app.prepare().then(() => {
    const isResource = (pathname: string) =>
        pathname.startsWith('/_next/') || pathname.startsWith('/static/') || pathname === "favicon.ico"

    createServer((req: IncomingMessage, res: ServerResponse) => {
        // Be sure to pass `true` as the second argument to `url.parse`.
        // This tells it to parse the query portion of the URL.
        const parsedUrl = parse(req.url ? req.url : "", true)
        const {pathname, query} = parsedUrl

        if (pathname === '/') {
            app.render(req, res, '/', query)
        } else if (isResource(pathname ? pathname : "")) {
            nextJsRequestHandler(req, res, parsedUrl)
        } else {
            redirectHandler(req, res, parsedUrl)
        }
    }).listen(3000, () => {
        console.log('> Ready on http://localhost:3000')
    })
})
