import * as React from "react";
import {useState} from "react";
import {Icon, Layout, Menu} from 'antd';
import {CreateMapping} from "./generated/CreateMapping";
import InputComponent from "./InputComponent";
import ResultsComponent from "./ResultsComponent";
import {SelectParam} from "antd/es/menu";
import {graphiqlUrl} from "./Config";

const {Header, Sider, Content} = Layout;

const UrlShrinkPage = () => {
        const [mapping, setMapping] = useState<CreateMapping>();
        const [key, setKey] = useState<String>("1");

        const menuItemSelected = ({key}: SelectParam) => {
            setKey(key)
        }

        let content;
        switch (key) {
            case "1":
                content = (mapping) ?
                    <ResultsComponent mapping={mapping}/> : <InputComponent onMappingCreated={setMapping}/>
                break
            case "2":
                content = <iframe style={{height: '100%', width: '100%'}} src={graphiqlUrl}/>
                break
            case "3":
                content = <div>
                    <h1>About UrlShrink</h1>
                    UrlShrink is a sample app that allows encoding of long URLs into shorter ones.
                    <p/>

                    <h2>Source Repository</h2>
                    <ul>
                        <li><a href="https://gitlab.com/lesnovell/urlshrink">Gitlab https://gitlab.com/lesnovell/urlshrink</a></li>
                    </ul>

                    <h2>Backend Details</h2>
                    <ul>
                        <li>Language: Java</li>
                        <li>Tools: Spring</li>
                        <li>Database: Redis</li>
                        <li>Web Service: GraphQL and REST available. GraphQL documentation is accessible from the <b>GraphiQL</b> tab on the left.</li>
                    </ul>

                    <h2>Frontend Details</h2>
                    <ul>
                        <li>Language: Typescript</li>
                        <li>Tools: NodeJS, React, NextJS</li>
                        <li>GraphQL: Uses Apollo GraphQL to connect to the backend</li>
                    </ul>

                    <h2>DevOps Details</h2>
                    <ul>
                        <li>Gitlab automated CI builds, dockerizes, and publishes releases</li>
                        <li>Terraform config to create VPC, Redis Database, EC2 for front-end, and EC2 for back-end</li>
                    </ul>
                </div>
                break
        }

        return (
            <div className="page">
                <Layout style={{height: '100%'}} hasSider={true}>
                    <Sider trigger={null}>
                        <div className="logo">
                            <span className="title">UrlShrink</span>
                        </div>
                        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} onSelect={menuItemSelected}>
                            <Menu.Item key="1">
                                <Icon type="shrink"/>
                                <span>Shrink URL</span>
                            </Menu.Item>
                            <Menu.Item key="2">
                                <Icon type="edit"/>
                                <span>GraphiQL</span>
                            </Menu.Item>
                            <Menu.Item key="3">
                                <Icon type="info-circle"/>
                                <span>About</span>
                            </Menu.Item>
                        </Menu>
                    </Sider>

                    <Content>
                        <div className="content">
                        {content}
                        </div>
                    </Content>
                </Layout>

                <style jsx>{`
                .logo {
                    height: 80px;
                    padding-left: 40px;
                    padding-top: 15px;
                }
                .layout, .page {
                    width: 100%;
                    height: 100%;
                }
                .title {
                    padding: 0px;
                    font-size: 30px;
                    color: white;
                    height: 5px;
                    vertical-align: middle;
                }
                .subtitle {
                    font-size: 18px;
                    color: rgba(150, 150, 200, 1);
                    vertical-align: bottom;
                }
                .content {
                    padding: 50px;
                    height: 100%;
                    width: 100%;
                }
            `}</style>
            </div>
        );
    }
;

export default UrlShrinkPage;