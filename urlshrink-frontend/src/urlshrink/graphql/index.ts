import {ApolloClient, InMemoryCache} from 'apollo-boost';
import {createHttpLink} from "apollo-link-http";
import 'isomorphic-fetch';
import {gqlUrl} from "../Config";

export const link = createHttpLink({
    uri: gqlUrl
});

export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link,
    resolvers: {}
});
