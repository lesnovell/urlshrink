import {client} from "./graphql"
import {GetMapping} from "./generated/GetMapping"
import GetMappingQuery from "./GetMappingQuery"
import {IncomingMessage, ServerResponse} from "http";
import {UrlWithParsedQuery} from "url";

export const redirectHandler = (req: IncomingMessage, res: ServerResponse, parsedUrl: UrlWithParsedQuery) => {
    const {pathname = ''} = parsedUrl

    client.query<GetMapping>({
        query: GetMappingQuery,
        variables: {shortPath: pathname.replace('/', '')}
    }).then(response => {
        const redirectUrl = response.data.getMapping.longUrl || '';
        res.writeHead(302, {Location: redirectUrl});
        res.end();
    }).catch(reason => {
        res.writeHead(404);
        res.end();
    })
}
