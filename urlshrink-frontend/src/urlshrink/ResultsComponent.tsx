import * as React from "react";
import {CreateMapping} from "./generated/CreateMapping";
import {baseUrl} from "./Config";

export type ResultsComponentProps = {
    mapping: CreateMapping | undefined
}

const ResultsComponent = ({mapping}: ResultsComponentProps) => {
    if (mapping) {
        return <div>
            Your short URL is <b>{baseUrl}/{mapping.createMapping.shortPath}</b>.
        </div>
    } else return <div/>
};

export default ResultsComponent;