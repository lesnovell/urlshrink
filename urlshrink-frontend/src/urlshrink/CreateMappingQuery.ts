import { gql } from 'apollo-boost'

export default gql`
    mutation CreateMapping($longUrl: String!) {
        createMapping(longUrl: $longUrl) {
            longUrl
            shortPath
            useCount
        }
    }`