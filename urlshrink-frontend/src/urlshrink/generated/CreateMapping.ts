/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateMapping
// ====================================================

export interface CreateMapping_createMapping {
  __typename: "ShortUrlMapping";
  longUrl: string | null;
  shortPath: string;
  useCount: number | null;
}

export interface CreateMapping {
  createMapping: CreateMapping_createMapping;
}

export interface CreateMappingVariables {
  longUrl: string;
}
