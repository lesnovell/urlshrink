/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetMapping
// ====================================================

export interface GetMapping_getMapping {
  __typename: "ShortUrlMapping";
  longUrl: string | null;
}

export interface GetMapping {
  getMapping: GetMapping_getMapping;
}

export interface GetMappingVariables {
  shortPath: string;
}
