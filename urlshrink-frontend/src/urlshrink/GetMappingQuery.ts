import { gql } from 'apollo-boost'

export default gql`
    query GetMapping($shortPath: String!) {
        getMapping(shortPath: $shortPath) {
            longUrl
        }
    }`