import * as React from "react";
import {Button, Icon, Input} from "antd";
import {useMutation} from "@apollo/react-hooks";
import {CreateMapping} from "./generated/CreateMapping";
import CreateMappingQuery from "./CreateMappingQuery";
import {validURL} from "./Helpers";
import {Row, Col} from 'antd';

export type MappingCreatedCallback = (data: CreateMapping) => void;
export type InputComponentProps = {
    onMappingCreated: MappingCreatedCallback
}


const InputComponent: React.FunctionComponent<InputComponentProps> =
    ({onMappingCreated}) => {
        const [url, setUrl] = React.useState<string>('');
        const [isValidUrl, setIsValidUrl] = React.useState<boolean>(false);
        const [createMapping, {data}] = useMutation<CreateMapping>(CreateMappingQuery);

        if (isValidUrl && data) {
            onMappingCreated(data);
        }

        const longUrlChanged = (e: React.FormEvent<HTMLInputElement>) => {
            setUrl(e.currentTarget.value);
            setIsValidUrl(validURL(e.currentTarget.value));
        };

        const generateShortUrl: React.MouseEventHandler<HTMLElement> = (e) => {
            createMapping({variables: {longUrl: url}});
            e.preventDefault();
        };

        const enterPressed: React.KeyboardEventHandler<HTMLInputElement> = (e) => {
            createMapping({variables: {longUrl: url}});
        };

        return (
            <div>
                <Row gutter={[10,20]}>
                    <Col>
                        Enter any valid URL, and we'll give you a short url
                    </Col>
                </Row>
                <Row gutter={[10,20]}>
                    <Col span={10}>
                        <Input prefix={(<Icon type={isValidUrl ? "check" : "exclamation-circle"}/>)}
                               placeholder="Long URL" onChange={longUrlChanged} onPressEnter={enterPressed}/>
                    </Col>
                    <Col span={1}>
                        <Button type="primary" disabled={!isValidUrl} onClick={generateShortUrl}>
                            Create Short URL
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    };

export default InputComponent;