export const baseUrl : string =
    process.env.BASE_URL || 'http://urlshrink.novell.tech'

export const gqlUrl : string =
    process.env.GQL_URL || 'http://tf-lb-20200215010928816800000001-368337427.us-east-1.elb.amazonaws.com/graphql'

export const graphiqlUrl : string =
    process.env.GRAPHIQL_URL || 'http://tf-lb-20200215010928816800000001-368337427.us-east-1.elb.amazonaws.com/graphiql'
