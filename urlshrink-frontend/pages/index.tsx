import * as React from "react";
import {NextPage} from "next";
import UrlShrinkPage from "../src/urlshrink/UrlShrinkPage";

const Home: NextPage<any> = () => {
    return <UrlShrinkPage/>;
};

export default UrlShrinkPage;