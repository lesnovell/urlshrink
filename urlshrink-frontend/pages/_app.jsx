import * as React from "react";
import {Component} from "react";
import {ApolloProvider} from '@apollo/react-hooks';
import {client} from "../src/urlshrink/graphql"
import 'antd/dist/antd.css';
import './page.css';


export default function MyApp({Component, pageProps}) {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
}