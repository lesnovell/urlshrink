#!/usr/bin/env bash
yarn run apollo schema:download --endpoint=http://localhost:8080/graphql graphql-schema.json
yarn run apollo codegen:generate \
--localSchemaFile=graphql-schema.json \
--excludes=node_modules/* \
--queries=src/urlshrink/*.ts \
--endpoint http://localhost:8080/graphql \
--target typescript \
--tagName=gql \
--addTypename \
--outputFlat \
src/urlshrink/generated
